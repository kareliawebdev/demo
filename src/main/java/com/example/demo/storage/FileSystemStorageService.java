package com.example.demo.storage;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileSystemStorageService implements StorageInterface {
	
	private final Path rootlocation;
	
	@Autowired
	public FileSystemStorageService(StorageProperties properties){
		if ( properties.getLocation().trim().length() == 0 ) {
			throw new StorageException("Location for upload file can't be empty");
		}
		this.rootlocation = Paths.get(properties.getLocation());
	}

	@Override
	public void init() {
		try {
			Files.createDirectories(rootlocation);
		}
		catch (IOException e) {
			throw new StorageException("Couldn't initialize storage", e);
		}
		
	}

	@Override
	public void store(MultipartFile file, String fileNumber) {
		try {
			
			if ( file.isEmpty() ) {
				throw new StorageException("File isEmpty returns true.");
			}
			
			Path destination = this.rootlocation.resolve(
					Paths.get( "import"+ fileNumber +".xls" ))
						.normalize()
						.toAbsolutePath();
			
			
			if (!destination.getParent().equals(this.rootlocation.toAbsolutePath())) {
				throw new StorageException("File cannot be stored outside directory.");
			}
			
			try (InputStream input = file.getInputStream()) {
				Files.copy(input,  destination, StandardCopyOption.REPLACE_EXISTING);
			}
		}
		catch(IOException e) {
			throw new StorageException("Fail to store the file", e);
		}
		
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootlocation,1)
					.filter(path -> !path.equals(this.rootlocation))
					.map(this.rootlocation::relativize);
		}
		catch (IOException e){
			throw new StorageException("Fail to read stored files", e);
		}
	}

	@Override
	public Path load(String filename) {
		return rootlocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) { 
				return resource;
			}
			else {
				throw new StorageFileNotFoundException("Couldn't read the file: "+filename);
			}
		}
		catch(MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read the file: " 
					+ filename, e);
		}

	}
	
	@Override
	public Path getRootlocation() {
		return this.rootlocation;
	}
}
