package com.example.demo.storage;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageInterface {
	void init();
	void store(MultipartFile file, String fileNumber);
	Stream<Path> loadAll();
	Path load(String filename);
	Resource loadAsResource(String filename);
	Path getRootlocation();
}
