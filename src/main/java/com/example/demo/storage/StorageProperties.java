package com.example.demo.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {
	
	private String location = "files";
	
	public String getLocation() {
		return location;
	}
}
