package com.example.demo.section;

import java.util.ArrayList;
import java.util.Iterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SectionsController {
	
	//private ArrayList <GeologicalClassRecord> geoClasses = new ArrayList<>();
	private final SectionRepository repository;
	
	@Autowired
	public SectionsController(SectionRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/sections/by-code")
	public SectionListRecord getSections(@RequestParam(value="code", defaultValue="default") String code) {
		
		ArrayList <SectionRecord> sections = new ArrayList<>();
		
		
		Iterator<Section> iterator = repository.findUsingCode(code).iterator();
		
		if (iterator.hasNext()) {
			
			while (iterator.hasNext()) {
				Section section = iterator.next();
				
				ArrayList <GeologicalClassRecord> geoClasses = new ArrayList<>();
				
				if (!section.getClass1Name().isEmpty()) geoClasses.add(
						new GeologicalClassRecord(section.getClass1Name(), section.getClass1Code()));
				
				if (!section.getClass2Name().isEmpty()) geoClasses.add(
						new GeologicalClassRecord(section.getClass2Name(), section.getClass2Code()));
				
				if (!section.getClassMName().isEmpty()) geoClasses.add(
						new GeologicalClassRecord(section.getClassMName(), section.getClassMCode()));
				
				sections.add(new SectionRecord(section.getSectionName(), geoClasses));
			}
			
			
		} 
			SectionListRecord sectionList = new SectionListRecord(sections);
			return sectionList;
		
	}
	
}
