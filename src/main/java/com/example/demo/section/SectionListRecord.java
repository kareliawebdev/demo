package com.example.demo.section;

import java.util.ArrayList;

public record SectionListRecord (ArrayList<SectionRecord> data) {

}
