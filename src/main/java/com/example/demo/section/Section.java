package com.example.demo.section;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Section {

	public Section() {}

	/**
	 * Section
	 * 
	 * @param sectionName
	 * @param class1Name
	 * @param class2Name
	 * @param classMName
	 * @param class1Code
	 * @param class2Code
	 * @param classMCode
	 */
	public Section(String sectionName, String class1Name, String class2Name, String classMName, String class1Code,
			String class2Code, String classMCode) {
		super();
		this.sectionName = sectionName;
		this.class1Name = class1Name;
		this.class2Name = class2Name;
		this.classMName = classMName;
		this.class1Code = class1Code;
		this.class2Code = class2Code;
		this.classMCode = classMCode;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String sectionName;
	private String class1Name;
	private String class2Name;
	private String classMName;
	private String class1Code;
	private String class2Code;
	private String classMCode;
	
	public Long getId() {
		return id;
	}

	public String getSectionName() {
		return sectionName;
	}

	public String getClass1Name() {
		return class1Name;
	}

	public String getClass2Name() {
		return class2Name;
	}

	public String getClassMName() {
		return classMName;
	}

	public String getClass1Code() {
		return class1Code;
	}

	public String getClass2Code() {
		return class2Code;
	}

	public String getClassMCode() {
		return classMCode;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public void setClass1Name(String class1Name) {
		this.class1Name = class1Name;
	}

	public void setClass2Name(String class2Name) {
		this.class2Name = class2Name;
	}

	public void setClassMName(String classMName) {
		this.classMName = classMName;
	}

	public void setClass1Code(String class1Code) {
		this.class1Code = class1Code;
	}

	public void setClass2Code(String class2Code) {
		this.class2Code = class2Code;
	}

	public void setClassMCode(String classMCode) {
		this.classMCode = classMCode;
	}
	
}
