package com.example.demo.section;

import java.util.ArrayList;

public record SectionRecord (String name, ArrayList<GeologicalClassRecord> geologicalClasses){
	
}
