package com.example.demo.section;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SectionRepository extends CrudRepository<Section, Long> {
	
	Section findById(long id);
	
	@Query("select s from Section s where s.class1Code = :code or s.class2Code = :code or s.classMCode= :code")
	Iterable<Section> findUsingCode(@Param("code") String code);
	
}
