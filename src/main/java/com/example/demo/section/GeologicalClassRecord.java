package com.example.demo.section;

public record GeologicalClassRecord (String name, String code) {

}
