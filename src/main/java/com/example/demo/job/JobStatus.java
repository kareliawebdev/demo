package com.example.demo.job;

public enum JobStatus {
	
	IN_PROGRESS,
	DONE,
	ERROR

}
