package com.example.demo.job;

public record JobStatusRecord (String jobId, String jobStatus) {

}
