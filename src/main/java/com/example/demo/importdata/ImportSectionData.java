package com.example.demo.importdata;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.example.demo.job.JobStatus;
import com.example.demo.section.Section;
import com.example.demo.section.SectionRepository;
import com.example.demo.storage.StorageInterface;

@Service
public class ImportSectionData {
	
	private ImportJobLogRepository jobLogRepository;
	private SectionRepository sectionRepository;
	private StorageInterface storageService;
	
	@Autowired
	public ImportSectionData(ImportJobLogRepository jobLogRepository, StorageInterface storageSercive,
			SectionRepository sectionRepository) {
		this.jobLogRepository = jobLogRepository;
		this.storageService = storageSercive;
		this.sectionRepository = sectionRepository;
	}

	@Async
	public void startImport(String filename, Long jobId) {
		
		try {
			
//			System.out.println("async import sleeps");
//			Thread.sleep(10000);
//			System.out.println("async import done with sleeping");
			
			Resource fileResource = storageService.loadAsResource(filename);
			InputStream file = fileResource.getInputStream();
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);
			DataFormatter formatter = new DataFormatter();
			Iterator<Row> iterator = sheet.iterator();
			
			Row headerRow = iterator.next();
			String[] headerValues = new String[headerRow.getPhysicalNumberOfCells()];
			
			for (int i = 0; i < headerRow.getPhysicalNumberOfCells(); i++) {
				headerValues[i] = formatter.formatCellValue(headerRow.getCell(i));
			}
			
			// Testing
			String[] compareValues = {"Section name","Class 1 name",
					"Class 1 code","Class 2 name","Class 2 code",
					"Class M name","Class M code"};
			
			if (Arrays.equals(headerValues, compareValues)) System.out.println("excel file header row ok");
	
			while(iterator.hasNext()) {
				Row row = iterator.next();
				Section section = new Section();
				section.setSectionName(formatter.formatCellValue(row.getCell(0)));
				section.setClass1Name(formatter.formatCellValue(row.getCell(1)));
				section.setClass1Code(formatter.formatCellValue(row.getCell(2)));
				section.setClass2Name(formatter.formatCellValue(row.getCell(3)));
				section.setClass2Code(formatter.formatCellValue(row.getCell(4)));
				section.setClassMName(formatter.formatCellValue(row.getCell(5)));
				section.setClassMCode(formatter.formatCellValue(row.getCell(6)));
				
				sectionRepository.save(section);
			}
			
			workbook.close();
			file.close();
			
			ImportJobLog jobLog =this.jobLogRepository.findById(jobId.longValue());
			jobLog.setStatus(JobStatus.DONE.name());
			this.jobLogRepository.save(jobLog);
			
		}catch(Exception e) {
			
			System.out.println("startImport method: " + e.getMessage());
			ImportJobLog jobLog = jobLogRepository.findById(jobId.longValue());
			jobLog.setStatus(JobStatus.ERROR.toString());
			jobLogRepository.save(jobLog);			
		}
	}
}