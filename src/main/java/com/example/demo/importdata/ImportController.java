package com.example.demo.importdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.example.demo.job.JobIdRecord;
import com.example.demo.job.JobStatus;
import com.example.demo.job.JobStatusRecord;
import com.example.demo.storage.*;

@RestController
public class ImportController {
	private final StorageInterface storageService;
	private final ImportJobLogRepository jobLogRepository;
	private final ImportSectionData importer;
	
	@Autowired
	public ImportController (StorageInterface storageService, ImportJobLogRepository jobLogRepository,
			ImportSectionData importer) {
		this.storageService = storageService;
		this.jobLogRepository = jobLogRepository;
		this.importer = importer;
	}
	
	@PostMapping("/import")
	public JobIdRecord handleImportFile(@RequestParam("file") MultipartFile file) {
		
		ImportJobLog jobLog = jobLogRepository.save(new ImportJobLog(JobStatus.IN_PROGRESS.name()));
		storageService.store(file, jobLog.getId().toString());
		jobLog.setFilename("import"+ jobLog.getId().toString() +".xls");
		jobLogRepository.save(jobLog);

		importer.startImport(jobLog.getFilename(), jobLog.getId());
		
		return new JobIdRecord(Long.toString(jobLog.getId()));
	}

	@GetMapping("/import/{id}")
	public JobStatusRecord getJobStatusById(@PathVariable String id) {
		
		ImportJobLog jobLog = jobLogRepository.findById(Long.valueOf(id).longValue());
		
		if(jobLog != null) {
			
			return new JobStatusRecord(jobLog.getId().toString(), jobLog.getStatus());
		
		}
		else return new JobStatusRecord(id, "Job ID not found");
	}
}