package com.example.demo.importdata;

import org.springframework.data.repository.CrudRepository;

public interface ImportJobLogRepository extends CrudRepository<ImportJobLog, Long>{
	
	ImportJobLog findById(long id);
	
}