package com.example.demo.exportdata;

import org.springframework.data.repository.CrudRepository;

public interface ExportJobLogRepository extends CrudRepository<ExportJobLog, Long>{
	
	ExportJobLog findById(long id);
	
}