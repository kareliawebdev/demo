package com.example.demo.exportdata;

public class ExportInProgressException extends RuntimeException {
	public ExportInProgressException (String message) {
		super (message);
	}
}
