package com.example.demo.exportdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.job.JobIdRecord;
import com.example.demo.job.JobStatus;
import com.example.demo.job.JobStatusRecord;
import com.example.demo.storage.*;

@RestController
public class ExportController {
	
	private final StorageInterface storageService;
	private final ExportJobLogRepository jobLogRepository;
	private final ExportSectionData exporter;

	@Autowired
	public ExportController (StorageInterface storageService, ExportJobLogRepository jobLogRepository,
			ExportSectionData exporter) {
		this.storageService = storageService;
		this.jobLogRepository = jobLogRepository;
		this.exporter = exporter;
	}
	
	@GetMapping("/export")
	public JobIdRecord handleExportFile() {
		
		ExportJobLog jobLog = jobLogRepository.save(new ExportJobLog(JobStatus.IN_PROGRESS.name()));
		jobLog.setFilename("export"+ jobLog.getId().toString() +".xls");
		jobLogRepository.save(jobLog);

		exporter.startExport(jobLog.getFilename(), jobLog.getId());
		
		return new JobIdRecord(Long.toString(jobLog.getId()));
	}

	@GetMapping("/export/{id}")
	public JobStatusRecord getJobStatusById(@PathVariable String id) {
		
		ExportJobLog jobLog = jobLogRepository.findById(Long.valueOf(id).longValue());
		
		if(jobLog != null) {
			
			return new JobStatusRecord(jobLog.getId().toString(), jobLog.getStatus());
		
		}
		else return new JobStatusRecord(id, "Job ID not found");
	}
	
	@GetMapping("/export/{id}/file")
	public ResponseEntity<Resource> getFile(@PathVariable String id) {
		
		ExportJobLog jobLog = jobLogRepository.findById(Long.valueOf(id).longValue());
		
		if (jobLog == null) return ResponseEntity.notFound().build();
		
		if (jobLog.getStatus().equals(JobStatus.IN_PROGRESS.name())) 
			throw new ExportInProgressException("getFile method called while export job IN_PROGRESS state.");
		
		Resource file = storageService.loadAsResource(jobLog.getFilename());
		if (file == null) return ResponseEntity.notFound().build();
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}
}