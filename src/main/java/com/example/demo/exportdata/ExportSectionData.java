package com.example.demo.exportdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.io.FileOutputStream;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import com.example.demo.job.JobStatus;
import com.example.demo.section.Section;
import com.example.demo.section.SectionRepository;
import com.example.demo.storage.StorageInterface;

@Service
public class ExportSectionData {
	
	private ExportJobLogRepository jobLogRepository;
	private SectionRepository sectionRepository;
	private StorageInterface storageService;
	
	@Autowired
	public ExportSectionData(ExportJobLogRepository jobLogRepository, SectionRepository sectionRepository,
			StorageInterface storageService)
	{
		this.jobLogRepository = jobLogRepository;
		this.sectionRepository = sectionRepository;
		this.storageService = storageService;
	}

	@Async
	public void startExport(String filename, Long jobId) {
		
		try {

//			System.out.println("async exporter service sleeps");
//			Thread.sleep(10000);
//			System.out.println("async exporter service is done sleeping");
			
			Workbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.createSheet();
			
			insertRows(getAllSectionData(), sheet);
			
			String outputFilename = storageService.getRootlocation().resolve(filename).toString();
			
			try (FileOutputStream outputStream = new FileOutputStream(outputFilename)){
				workbook.write(outputStream);
				workbook.close();
				ExportJobLog jobLog = jobLogRepository.findById(jobId.longValue());
				jobLog.setStatus(JobStatus.DONE.toString());
				jobLogRepository.save(jobLog);
			}
			
		}catch(Exception e) {
			
			System.out.println("startExport method: " + e.getMessage());
			ExportJobLog jobLog = jobLogRepository.findById(jobId.longValue());
			jobLog.setStatus(JobStatus.ERROR.toString());
			jobLogRepository.save(jobLog);
			
		}
	}
	
	private Iterator<Section> getAllSectionData() {
		Iterator<Section> sections = sectionRepository.findAll().iterator();
		return sections;
		
	}
	
	private void insertRows (Iterator<Section> data, Sheet sheet) {

		Row headerRow = sheet.createRow(0);
		headerRow.createCell(0).setCellValue("Section name");
		headerRow.createCell(1).setCellValue("Class 1 name");
		headerRow.createCell(2).setCellValue("Class 1 code");
		headerRow.createCell(3).setCellValue("Class 2 Name");
		headerRow.createCell(4).setCellValue("Class 2 code");
		headerRow.createCell(5).setCellValue("Class M Name");
		headerRow.createCell(6).setCellValue("Class M code");
		
		int rowIndex = 1;
		while(data.hasNext()) {
			
			Section section = data.next();
			
			Row row = sheet.createRow(rowIndex);
			
			Cell cell = row.createCell(0);
			cell.setCellValue(section.getSectionName());
			
			cell = row.createCell(1);
			cell.setCellValue(section.getClass1Name());
			
			cell = row.createCell(2);
			cell.setCellValue(section.getClass1Code());
			
			cell = row.createCell(3);
			cell.setCellValue(section.getClass2Name());
			
			cell = row.createCell(4);
			cell.setCellValue(section.getClass2Code());
			
			cell = row.createCell(5);
			cell.setCellValue(section.getClassMName());
			
			cell = row.createCell(6);
			cell.setCellValue(section.getClassMCode());
			
			rowIndex++;
		}
		
	}
}